<?php

namespace App\Tests\Entity;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase 
{
    public function testSetPseudo() 
    {
        $user = new User();
        $pseudo = $user->setPseudo("test pseudo");

        $this->assertEquals("test pseudo", $pseudo->getPseudo());
    }

    public function testSetEmail() 
    {
        $user = new User();
        $email = $user->setEmail("email");

        $this->assertEquals("email", $email->getEmail());
    }
}